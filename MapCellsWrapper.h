#pragma once
#include "MapCell.h"
#include <vector>
#include <optional>

namespace Map
{
	//does not let friends of MapLayout access cells directly
	class MapCellsWrapper
	{
	public:
		std::vector< MapCell const*> GetAdjacentCells(size_t x, size_t y) const;
		std::vector< MapCell const*> GetAdjacentCells(size_t index) const;

		std::optional<MapCell const*> GetCellToThe(size_t x, size_t y, MapCellSide side) const;
		std::optional<MapCell const*> GetCellToThe(size_t index, MapCellSide side) const;

		std::optional<size_t> GetCellIndexToThe(size_t x, size_t y, MapCellSide side) const;
		std::optional<size_t> GetCellIndexToThe(size_t index, MapCellSide side) const;

		const MapCell& operator[](std::size_t idx) const { return cells[idx]; }

		static constexpr size_t width = 15;			//Allows maps up to 130 cells
		static constexpr size_t height = 10;			//alter values if more is needed

	private:
		friend class MapLayout;

		std::vector< MapCell*> GetAdjacentCells(size_t x, size_t y);
		std::vector< MapCell*> GetAdjacentCells(size_t index);

		std::optional<MapCell*> GetCellToThe(size_t x, size_t y, MapCellSide side);
		std::optional<MapCell*> GetCellToThe(size_t index, MapCellSide side);

		MapCell& operator[](std::size_t idx) { return cells[idx]; }

		std::vector<MapCell>cells{ width * height };
	};
}