#include "MapCellsWrapper.h"
#include <cassert>
#include <utility>

using namespace Map;

std::vector<MapCell const*> MapCellsWrapper::GetAdjacentCells(size_t x, size_t y) const
{
	std::vector<MapCell const*> retVal;
	for (int i = 0; i < 4; i++)
	{
		auto side = static_cast<MapCellSide>(i);
		auto cellToTheSide = GetCellToThe(x, y, side);
		if (cellToTheSide.has_value())
			retVal.push_back(cellToTheSide.value());
		else
			retVal.push_back(&lockedCell);
	}
	return retVal;
}
std::vector<MapCell const*> MapCellsWrapper::GetAdjacentCells(size_t index) const
{
	return GetAdjacentCells(index % width, index / width);
}

std::optional<MapCell const *> MapCellsWrapper::GetCellToThe(size_t x, size_t y, MapCellSide side) const
{
	auto index = GetCellIndexToThe(x, y, side);
	if (index.has_value())
		return &cells[index.value()];
	return {};
}

std::optional<size_t> MapCellsWrapper::GetCellIndexToThe(size_t index, MapCellSide side) const
{
	return GetCellIndexToThe(index % width, index / width, side);
}

std::optional<MapCell const*> MapCellsWrapper::GetCellToThe(size_t index, MapCellSide side) const
{
	return GetCellToThe(index % width, index / width, side);
}

std::optional<size_t> MapCellsWrapper::GetCellIndexToThe(size_t x, size_t y, MapCellSide side) const
{
	switch (side)
	{
	case MapCellSide::Top:
		if (y < 1)return{};
		return x + (y - 1) * width;
		break;
	case MapCellSide::Bottom:
		if (y > height - 2)return{};
		return x + (y + 1) * width;
		break;
	case MapCellSide::Left:
		if (x < 1)return{};
		return (x - 1) + y * width;
		break;
	case MapCellSide::Right:
		if (x > width - 2)return{};
		return (x + 1) + y * width;
		break;
	default:
		assert(false && "Wrong MapCellSide passed to MapLayout::GetCellToThe");
		return{};
		break;
	}
}

std::vector<MapCell*> Map::MapCellsWrapper::GetAdjacentCells(size_t x, size_t y)
{
	auto constCells = static_cast<const MapCellsWrapper&>(*this).GetAdjacentCells(x, y);
	std::vector<MapCell*>none_constCells;
	for (auto& cell : constCells) none_constCells.emplace_back(const_cast<MapCell*>(cell));
	return none_constCells;
}

std::vector<MapCell*> Map::MapCellsWrapper::GetAdjacentCells(size_t index)
{
	return GetAdjacentCells(index % width, index / width);
}

std::optional<MapCell*> Map::MapCellsWrapper::GetCellToThe(size_t x, size_t y, MapCellSide side)
{
	auto constCell = static_cast<const MapCellsWrapper&>(*this).GetCellToThe(x, y, side);
	if (constCell.has_value())
		return { const_cast<MapCell*>(constCell.value()) };
	else
		return {};
}

std::optional<MapCell*> Map::MapCellsWrapper::GetCellToThe(size_t index, MapCellSide side)
{
	return GetCellToThe(index % width, index / width, side);
}
