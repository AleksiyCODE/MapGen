#include "MapGenerator.h"
#include "MapLayout.h"

using namespace Map;
int main()
{
	for (;;)
	{
		auto layout = MapGenerator::GenerateMapLayout(
			{ 
				Level::FirstCave,
				15,
				{
					{Interactable::Chest, 3},
					{Interactable::Exit, 2},
					{Interactable::Camp, 3}
				}
			});
		if(layout)
			layout->PrintLayout();
		
		std::getchar();
	}

	return 0;
}