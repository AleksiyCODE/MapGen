#pragma once
#include <optional>
#include <vector>
#include "RandomUnit.h"

class Util
{
public:
	template <typename T>
	static std::optional<T> PopRandomVectorElement(std::vector<T>& arr)
	{
		if (arr.size() != 0)
		{
			size_t chosenIndex = RU.GetUInt(0u, arr.size() - 1);
			size_t chosenValue = arr[chosenIndex];
			arr.erase(arr.begin() + chosenIndex);
			return { chosenValue };
		}
		else
		{
			return{};
		}
	}

};