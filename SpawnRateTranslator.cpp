#include "SpawnRateTranslator.h"
#include "RandomUnit.h"
//#include <algorithm>

using namespace Map;

template <typename CumulativeStorage>
decltype(auto) Map::SpawnRateTranslator::LookupStorage(const CumulativeStorage&cumWeights)
{
	float val = RU.GetFloat(0.0f, cumWeights.back().second);
	auto firstLargerThan = std::find_if(cumWeights.begin(), cumWeights.end(), [val](const auto& pair) {return pair.second > val; });
	return (firstLargerThan)->first;
}

Enemy Map::SpawnRateTranslator::DecideEnemyTypeSpawn(Level level, Interactable inter)
{
	auto& cumWeights = enemyCumulativeWeightsStorage[level][inter];
	//check if storage was already filled
	if (cumWeights.size())
	{
		return LookupStorage(cumWeights);
	}
	else
	{
		FillStorage(cumWeights, GetSpawnRates(level)[inter]);
		return LookupStorage(cumWeights);
	}
}

Trap Map::SpawnRateTranslator::DecideTrapTypeSpawn(Level level)
{
	auto& cumWeights = trapCumulativeWeightsStorage[level];
	//check if storage was already filled
	if (cumWeights.size())
	{
		return LookupStorage(cumWeights);
	}
	else
	{
		FillStorage(cumWeights, GetSpawnRates(level).trapSpawnRates);
		return LookupStorage(cumWeights);
	}
}

template <typename CumulativeStorage, typename Storage>
void Map::SpawnRateTranslator::FillStorage(CumulativeStorage&cumWeights, const Storage& weights)
{
	if (weights.size() == 0)
	{
		assert(false && "Incomplete enemy weight information");
	}
	float cumulatedWeight = 0;
	for (const auto& pair : weights)
	{
		cumulatedWeight += pair.second;
		cumWeights.emplace_back(pair.first, cumulatedWeight);
	}
}

std::unordered_map<Level, std::unordered_map<Interactable, std::vector<std::pair<Enemy, float>>>> SpawnRateTranslator::enemyCumulativeWeightsStorage;
std::unordered_map<Level, std::vector<std::pair<Trap, float>>> SpawnRateTranslator::trapCumulativeWeightsStorage;