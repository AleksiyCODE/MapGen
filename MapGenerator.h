#pragma once
#include <memory>
#include <vector>
#include <tuple>
#include "MapGenerationData.h"

namespace Map
{
	class MapLayout;
	class MapCell;
	class MapCellPlacingInfo;

	//Creates maps with given parameters
	class MapGenerator
	{
	public:
		static std::unique_ptr<MapLayout> GenerateMapLayout(const MapGenerationData& generationParameters);
	private:
		static MapCell CreateRandomCell(const std::vector< MapCell const* >& adjacentCells);
		static MapCell CreateLockedCell(const std::vector< MapCell const* >& adjacentCells);
		static MapCellPlacingInfo CreateCell(const std::vector< MapCell const* >& adjacentCells);	//MapCellSides are to be determened later

		static void PlaceStart(MapLayout& layout);
		static void SpawnEnemiesOnInteractables(MapLayout& layout, Level level);
		template <typename T>
		static void PopulateWithRandomT(MapLayout& layout, Level level);
		static MapCellPlacementResult PlaceInteractables(MapLayout& layout, const MapGenerationData& generationData);
		static void ResetGeneration(std::unique_ptr<MapLayout>& layout, size_t& roomsAdded);
		static size_t GetRequieredInteractables(const MapGenerationData& generationData);
	};
}