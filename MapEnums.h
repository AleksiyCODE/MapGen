#pragma once

namespace Map
{
	enum class MapCellStatus
	{
		Empty,
		Placed
	};

	enum class MapCellSideStatus
	{
		Empty,
		Blocked,
		Trapped
	};

	enum class MapCellPlacementResult
	{
		Success,
		Fail
	};

	enum class MapCellSide : size_t
	{
		Top = 0,
		Bottom = 1,
		Left = 2,
		Right = 3
	};

	enum class Interactable
	{
		Chest,
		Camp,
		Start,
		Exit,
		Empty
	};

	enum class Enemy
	{
		Ghoul,
		Hound,
		Wretch,
		None
	};

	enum class Trap
	{
		Spikes,
		DartTrap,
		None
	};

	enum class Level
	{
		FirstCave,
		SecondCave
	};
}