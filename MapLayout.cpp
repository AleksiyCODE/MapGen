﻿#include "MapLayout.h"
#include <algorithm>
#include <assert.h>
#include <algorithm>
#include <utility>
#include "RandomUnit.h"
#include "Utility.h"
#include <iostream>			//for debugging

using namespace Map;

void MapLayout::SetCell(size_t index, MapCell cell)
{
	cells[index] = cell;
	avalibleForPlacement.erase(
		std::remove(avalibleForPlacement.begin(), avalibleForPlacement.end(), index),
		avalibleForPlacement.end()
	);
	if (cell.GetOpenSideCount() == 1) emptyDeadEnds.push_back(index);
	for (int i = 0; i < 4; i++)
	{
		auto side = static_cast<MapCellSide>(i);
		auto adjacent = cells.GetCellToThe(index, side);
		auto adjacentIndex = cells.GetCellIndexToThe(index, side);
		if (adjacent.has_value())
			if (cell.GetSide(side) != MapCellSideStatus::Blocked && adjacent.value()->GetStatus() == MapCellStatus::Empty)
				if (std::find(avalibleForPlacement.begin(), avalibleForPlacement.end(), adjacentIndex.value()) == avalibleForPlacement.end())
				{
					avalibleForPlacement.push_back(adjacentIndex.value());
				}
	}
}
void MapLayout::SetCell(size_t x, size_t y, MapCell cell)
{
	SetCell(x + y * cells.width, cell);
}


std::optional<size_t> MapLayout::PopRandomAvalibleForPlacement()
{
	return Util::PopRandomVectorElement(avalibleForPlacement);
}

std::optional<size_t> MapLayout::PopRandomEmptyDeadEnd()
{
	return Util::PopRandomVectorElement(emptyDeadEnds);
}

void MapLayout::SetCellInteractable(size_t index, Interactable inter)
{
	//remove previous interactable that was in ihis place
	auto range = interactables.equal_range(inter);
	for (auto it = range.first; it != range.second; ++it) {
		if (it->second == index)
			interactables.erase(it);
		break;
	}

	cells[index].interactable = inter;
	if (inter != Interactable::Empty)
	{
		interactables.emplace(inter, index);
	}
}

void Map::MapLayout::SpawnEnemy(size_t cellIndex, Enemy enemy)
{
	if (enemy != Enemy::None)
	{
		auto& target = cells[cellIndex];
		if (!target.CanSpawnEnemy())
		{	//some methods will try to blindly set enemies everywhere, so silently do nothing
			return;
		}
		target.SetEnemy(enemy);
		PreventEnemySurrounding(cellIndex);
	}
}

void Map::MapLayout::SpawnTrapRandomDir(size_t cellIndex, Trap trap)
{
	if (trap != Trap::None)
	{
		cells[cellIndex].SetTrap(trap, static_cast<MapCellSide>(RU.GetUInt(0, 3)));		
	}
}

//helps avoid situations where player is attacked simultaneusly from two sides
void Map::MapLayout::PreventEnemySurrounding(size_t index)
{
	auto& initial = cells[index];
	for (size_t i = 0; i < initial.sides.size(); ++i)
	{
		//if there is a corridor on that side and not blocking that room can lead to surrounding
		if (initial.sides[i] != MapCellSideStatus::Blocked && cells.GetCellToThe(index, static_cast<MapCellSide>(i)).value()->GetOpenSideCount()>2)
		{
			DisableEnemySpawnAround(cells.GetCellIndexToThe(index, static_cast<MapCellSide>(i)).value());
		}
	}
}

void Map::MapLayout::DisableEnemySpawnAround(size_t index)
{
	auto adjacent = cells.GetAdjacentCells(index);
	auto& curCell = cells[index];
	for (size_t i = 0; i < adjacent.size(); ++i)
	{
		if (curCell.GetSide(static_cast<MapCellSide>(i)) != MapCellSideStatus::Blocked)
			cells.GetCellToThe(index, static_cast<MapCellSide>(i)).value()->SetEnemySpawnAllowed(false);
	}
}

void Map::MapLayout::FinalizeLayoutCreation()
{
	//clear entrance
	cells.GetCellToThe(startInfo.startIndex, startInfo.entrance).value()->enemy = Enemy::None;
}

MapCellPlacementResult MapLayout::PlaceStart(size_t x, size_t y, MapCellSide entrance)
{
	if (!startPlaced)
	{
		if (PrepareStartPlacing(x, y, entrance) == MapCellPlacementResult::Success)
		{
			startInfo.startIndex = x + y * cells.width;
			startInfo.entrance = entrance;
			auto& startCell = cells[startInfo.startIndex];
			startCell.SetStatus(MapCellStatus::Placed);
			startCell.SetInteractable(Interactable::Start);
			//SetCellInteractable(x + y * cells.width, Interactable::Start);	//this version is better if there are more interactables in game with 0% chance of having enemies
																				//but tweaks are needed for it to work, because SpawnRates do not accout for Start 
			for (int i = 0; i < 4; i++)
			{
				auto side = static_cast<MapCellSide>(i);
				startCell.SetSide(side,
					side == entrance ? MapCellSideStatus::Empty : MapCellSideStatus::Blocked);
			}
			startPlaced = true;
			return MapCellPlacementResult::Success;
		}
	}
	else
	{
		assert(false && ("start already placed"));
	}
	return MapCellPlacementResult::Fail;
}

void MapLayout::PrintLayout()
{
	std::vector<std::string> output;
	output.resize(cells.height * 3);
	for (int i = 0; i < cells.width; i++)
	{
		for (int j = 0; j < cells.height; j++)
		{
			const auto& cell = cells[i + j * cells.width];
			if (cell.GetStatus() == MapCellStatus::Empty)
			{
				output[j * 3].append("   ");
				output[j * 3 + 1].append("   ");
				output[j * 3 + 2].append("   ");
			}
			else
			{
				switch (cell.GetSide(MapCellSide::Top))
				{
				case MapCellSideStatus::Blocked:
					output[j * 3].append("\xDB\xDB\xDB");
					break;
				case  MapCellSideStatus::Trapped:
					output[j * 3].append("\xDB.\xDB");
					break;
				case MapCellSideStatus::Empty:
					output[j * 3].append("\xDB \xDB");
					break;
				default:
					assert(false && "Not implemented for this cell status");
					break;
				}
				switch (cell.GetSide(MapCellSide::Left))
				{
				case MapCellSideStatus::Blocked:
					output[j * 3 + 1].append("\xDB");
					break;
				case  MapCellSideStatus::Trapped:
					output[j * 3 + 1].append(".");
					break;
				case MapCellSideStatus::Empty:
					output[j * 3 + 1].append(" ");
					break;
				default:
					assert(false && "Not implemented for this cell status");
					break;
				}				
				//
				if (cell.GetInteractable() != Interactable::Empty && cell.GetEnemy() != Enemy::None)
				{
					output[j * 3 + 1].append("+");
				}
				else
				{
					if (cell.GetEnemy() != Enemy::None)
					{
						switch (cell.GetEnemy())
						{
						case Enemy::Ghoul:
							output[j * 3 + 1].append("G");
							break;
						case Enemy::Hound:
							output[j * 3 + 1].append("H");
							break;
						case Enemy::Wretch:
							output[j * 3 + 1].append("W");
							break;
						default:
							assert(false && "Enemy not implemented");
							output[j * 3 + 1].append("?");
							break;
						}
					}
					else
					{
						switch (cell.GetInteractable())
						{
						case Interactable::Empty:
							//if (cell.enemySpawnAllowed)
								output[j * 3 + 1].append(" ");
							//else
							//	output[j * 3 + 1].append("-");
							break;
						case Interactable::Camp:
							output[j * 3 + 1].append("X");
							break;
						case Interactable::Chest:
							output[j * 3 + 1].append("C");
							break;
						case Interactable::Exit:
							output[j * 3 + 1].append("E");
							break;
						case Interactable::Start:
							output[j * 3 + 1].append("S");
							break;
						default:
							assert(false && "Interactable not implemented");
							output[j * 3 + 1].append("?");
							break;
						}
					}
				}
				//
				switch (cell.GetSide(MapCellSide::Right))
				{
				case MapCellSideStatus::Blocked:
					output[j * 3 + 1].append("\xDB");
					break;
				case  MapCellSideStatus::Trapped:
					output[j * 3 + 1].append(".");
					break;
				case MapCellSideStatus::Empty:
					output[j * 3 + 1].append(" ");
					break;
				default:
					assert(false && "Not implemented for this cell status");
					break;
				}
				switch (cell.GetSide(MapCellSide::Bottom))
				{
				case MapCellSideStatus::Blocked:
					output[j * 3 + 2].append("\xDB\xDB\xDB");
					break;
				case  MapCellSideStatus::Trapped:
					output[j * 3 + 2].append("\xDB.\xDB");
					break;
				case MapCellSideStatus::Empty:
					output[j * 3 + 2].append("\xDB \xDB");
					break;
				default:
					assert(false && "Not implemented for this cell status");
					break;
				}
			}
		}
	}
	std::cout << "--------------------------------------\n";
	for (auto& s : output)
		std::cout << s << '\n';
	std::cout << "--------------------------------------\n";
	std::cout << "EmptyDeadEndsLeft " << emptyDeadEnds.size() << '\n';
}

bool MapLayout::CheckBounds(size_t x, size_t y) const
{
	return x >= 0 && y >= 0 && x < cells.width && y < cells.height;
}

MapCellPlacementResult MapLayout::PrepareStartPlacing(size_t x, size_t y, MapCellSide entrance)
{
	if (CheckBounds(x, y))				//if within bounds
	{
		auto corridor = cells.GetCellIndexToThe(x, y, entrance);
		if (corridor.has_value())		//corridor is within bounds
		{
			avalibleForPlacement.push_back(corridor.value());
			return MapCellPlacementResult::Success;
		}
	}
	return MapCellPlacementResult::Fail;
}