#include "MapCell.h"
#include <algorithm>

using namespace Map;

MapCellSideStatus MapCell::GetSide(MapCellSide side) const
{
	return sides[static_cast<size_t>(side)];
}

void MapCell::SetSide(MapCellSide side, MapCellSideStatus sideStatus)
{
	sides[static_cast<std::uint32_t>(side)] = sideStatus;
}

MapCellStatus MapCell::GetStatus() const
{
	return status;
}

void MapCell::SetStatus(MapCellStatus in_status)
{
	this->status = in_status;
}

Interactable MapCell::GetInteractable() const
{
	return interactable;
}

Enemy Map::MapCell::GetEnemy() const
{
	return enemy;
}

void Map::MapCell::SetEnemy(Enemy enem)
{
	enemy = enem;
	enemySpawnAllowed = false;
}

void Map::MapCell::SetTrap(Trap trap, MapCellSide side)
{
	auto sideIndex = static_cast<size_t>(side);
	if (sides[sideIndex] == MapCellSideStatus::Empty)
	{
		trapSides[sideIndex] = trap;
		sides[sideIndex] = MapCellSideStatus::Trapped;
	}
}

void MapCell::SetInteractable(Interactable in_interactable)
{
	this->interactable = in_interactable;
}

size_t MapCell::GetOpenSideCount() const
{
	return static_cast<size_t>(std::count_if(sides.begin(), sides.end(),
		[](MapCellSideStatus status) {return status != MapCellSideStatus::Blocked; }));
}

bool MapCell::CanSpawnEnemy() const
{
	return enemySpawnAllowed;
}

void MapCell::SetEnemySpawnAllowed(bool spawnAllowed)
{
	enemySpawnAllowed = spawnAllowed;
}

MapCellSide MapCell::GetOppositeSide(MapCellSide side)
{
	switch (side)
	{
	case MapCellSide::Top:
		return MapCellSide::Bottom;
		break;
	case MapCellSide::Bottom:
		return MapCellSide::Top;
		break;
	case MapCellSide::Left:
		return MapCellSide::Right;
		break;
	case MapCellSide::Right:
		return MapCellSide::Left;
		break;
	default:
		return MapCellSide::Top;
		break;
	}
}

LockedCell::LockedCell()
{
	for (auto& side : sides)
	{
		side = MapCellSideStatus::Blocked;
	}
	status = MapCellStatus::Placed;
	enemySpawnAllowed = false;
}

void Map::LockedCell::SetEnemySpawnAllowed(bool)
{
	//not allowing modification
}

void Map::LockedCell::SetEnemy(Enemy)
{
	//not allowing modification
}

void Map::LockedCell::SetStatus(MapCellStatus)
{
	//not allowing modification
}
