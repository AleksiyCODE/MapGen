#pragma once
#include <unordered_map>
#include "MapEnums.h"
#include <cassert>

namespace Map
{
	using EnemySpawnRates = std::unordered_map<Enemy, float>;
	using TrapSpawnRates = std::unordered_map<Trap, float>;

	class SpawnRates
	{
	protected:
		SpawnRates(const std::unordered_map<Interactable, EnemySpawnRates>& enemyRates, const TrapSpawnRates& trapSpawnRates) :
			enemySpawnRates(enemyRates),
			trapSpawnRates(trapSpawnRates)
		{};
	public:
		const EnemySpawnRates& operator[](Interactable key) const
		{
			return enemySpawnRates.at(key);
		}
		const std::unordered_map<Interactable, EnemySpawnRates> enemySpawnRates;
		const TrapSpawnRates trapSpawnRates;
	};

	//use GetEnemySpawnRates(Level level) to access this
	class FirstDungeonMESR : public SpawnRates
	{
	private:
		static const FirstDungeonMESR& GetInstance()
		{
			static FirstDungeonMESR instance;
			return instance;
		}
		friend const SpawnRates& GetSpawnRates(Level level);
		FirstDungeonMESR() :
			SpawnRates(
				std::unordered_map<Interactable, EnemySpawnRates>
		{
			{ Interactable::Empty, 
				{
					{Enemy::Ghoul, 1.0f},
					{Enemy::Hound, 2.0f},
					{Enemy::None, 20.0f}
				}
			},
			{ Interactable::Camp,
				{
					{Enemy::Ghoul, 1.0f},
					{Enemy::Hound, 1.0f},
					{Enemy::None, 50.0f}
				}
			},
			{ Interactable::Chest,
				{
					{Enemy::Ghoul, 5.0f},
					{Enemy::Hound, 1.0f},
					{Enemy::None, 0.5f}
				}
			},
			{ Interactable::Exit,
				{
					{Enemy::Ghoul, 1.0f},
					{Enemy::Hound, 5.0f},
					{Enemy::None, 5.0f}
				}
			}
		},
				TrapSpawnRates
			{
				{
					{Trap::Spikes, 2.0f},
					{Trap::DartTrap, 1.0f},
					{Trap::None, 30.0f}
				}
			}
				)
		{};
	};

	//use GetEnemySpawnRates(Level level) to access this
	class SecondDungeonMESR : public SpawnRates
	{
	private:
		static const SecondDungeonMESR& GetInstance()
		{
			static SecondDungeonMESR instance;
			return instance;
		}
		friend  const SpawnRates& GetSpawnRates(Level level);
		SecondDungeonMESR() :
			SpawnRates(
				std::unordered_map<Interactable, EnemySpawnRates>
		{
			{ Interactable::Empty, 
				{
					{Enemy::Ghoul, 1.0f},
					{Enemy::Hound, 2.0f},
					{Enemy::Wretch, 2.0f},
					{Enemy::None, 15.0f}
				}
			},
			{ Interactable::Camp, 
				{
					{Enemy::Wretch, 1.0f},
					{Enemy::None, 40.0f}
				}
			},
			{ Interactable::Chest, 
				{
					{Enemy::Ghoul, 5.0f},
					{Enemy::Wretch, 2.0f},
					{Enemy::None, 1.0f}
				}
			},
			{ Interactable::Exit, 
				{
					{Enemy::Wretch, 2.0f},
					{Enemy::None, 0.3f}
				}
			}
		},
			{
				{
					{Trap::Spikes, 1.0f},
					{Trap::DartTrap, 2.0f},
					{Trap::None, 20.0f}
				}
			}
			) {};
	};

	const SpawnRates& GetSpawnRates(Level level);
}