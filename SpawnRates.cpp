#include "SpawnRates.h"
#include <cassert>

using namespace Map;

const SpawnRates& Map::GetSpawnRates(Level level)
{
	switch (level)
	{
	case Map::Level::FirstCave:
		return FirstDungeonMESR::GetInstance();
		break;
	case Map::Level::SecondCave:
		return SecondDungeonMESR::GetInstance();
		break;
	default:
		assert(false && "GetSpawnRates can not give info about this level");
		return FirstDungeonMESR::GetInstance();
		break;
	}
}