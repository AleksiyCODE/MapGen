#include "MapGenerator.h"
#include "MapLayout.h"
#include "MapCellPlacingInfo.h"
#include <algorithm>
#include "RandomUnit.h"
#include <assert.h>
#include "SpawnRateTranslator.h"
#include <utility>
#include <type_traits>

#include <iostream>   //for debugging

using namespace Map;

std::unique_ptr<MapLayout> MapGenerator::GenerateMapLayout(const MapGenerationData& generationParameters)
{
	std::unique_ptr<MapLayout> layout;
	//checking generation parameters validity
	if (generationParameters.minRoomNumber > (layout->cells.width * layout->cells.height) - (layout->cells.width + layout->cells.height)
		||
		GetRequieredInteractables(generationParameters) > sqrt(generationParameters.minRoomNumber) * 3 + 1)
	{
		assert(false && "MapLayout can not be created for these MapGenerationData");
		layout.release();
		return layout;
	}


	auto minimumRoomNumber = generationParameters.minRoomNumber;
	size_t roomsAdded;
	bool generationFailed = true;
	do
	{
		ResetGeneration(layout, roomsAdded);

		//generating branching paths
		do
		{
			auto addingIndex = layout->PopRandomAvalibleForPlacement();
			if (!addingIndex.has_value())		//failed to generate, must try again
			{
				goto stopGeneration;
			}
			auto adding = CreateRandomCell(std::as_const(layout->cells).GetAdjacentCells(addingIndex.value()));
			layout->SetCell(addingIndex.value(), adding);
			roomsAdded++;
		} while (roomsAdded < minimumRoomNumber);

		//sealing off all exits
		for (;;)
		{
			auto fillingIndex = layout->PopRandomAvalibleForPlacement();
			if (fillingIndex.has_value())
			{
				layout->SetCell(fillingIndex.value(), CreateLockedCell(std::as_const(layout->cells).GetAdjacentCells(fillingIndex.value())));
			}
			else
			{
				break;		//all filled
			}
		}

		//populating with interactables
		if (PlaceInteractables(*layout.get(), generationParameters) == MapCellPlacementResult::Fail)
		{
			goto stopGeneration;			//generated layout could not fit all desiered interactibles
		}
		SpawnEnemiesOnInteractables(*layout.get(), generationParameters.level);
		PopulateWithRandomT<Enemy>(*layout.get(), generationParameters.level);
		PopulateWithRandomT<Trap>(*layout.get(), generationParameters.level);
		layout->FinalizeLayoutCreation();
		generationFailed = false;

	stopGeneration:;
	} while (generationFailed);
	return layout;
}


MapCell MapGenerator::CreateRandomCell(const std::vector< MapCell const* >& adjacentCells)
{
	auto cellInfo = CreateCell(adjacentCells);
	auto& adding = cellInfo.cell;
	auto& sidesToDecide = cellInfo.sidesToDecide;

	//don't allow new cell to stop branching
	if (sidesToDecide.size() == 0)		//nothing to decide
		return adding;
	if (sidesToDecide.size() == 1)		//if only one way to continue - let it branch further		
	{
		adding.SetSide(sidesToDecide[0], MapCellSideStatus::Empty);
		return adding;
	}

	size_t goingToBeBlocked = RU.GetUInt(0, sidesToDecide.size());

	std::vector<MapCellSideStatus>decidedSides;
	for (size_t i = 0; i < sidesToDecide.size(); i++)
	{
		if (i < goingToBeBlocked) decidedSides.push_back(MapCellSideStatus::Blocked);
		else					  decidedSides.push_back(MapCellSideStatus::Empty);
	}

	std::shuffle(decidedSides.begin(), decidedSides.end(), RU.GetMT19937());

	for (size_t i = 0; i < sidesToDecide.size(); ++i)
	{
		adding.SetSide(sidesToDecide[i], decidedSides[i]);
	}
	return adding;
}

MapCell MapGenerator::CreateLockedCell(const std::vector< MapCell const* >& adjacentCells)
{
	auto cellInfo = CreateCell(adjacentCells);
	for (size_t i = 0; i < cellInfo.sidesToDecide.size(); ++i)
	{
		cellInfo.cell.SetSide(cellInfo.sidesToDecide[i], MapCellSideStatus::Blocked);
	}
	return cellInfo.cell;
}

MapCellPlacingInfo MapGenerator::CreateCell(const std::vector< MapCell const* >& adjacentCells)
{
	MapCell adding{};
	adding.SetStatus(MapCellStatus::Placed);
	adding.SetEnemySpawnAllowed(true);
	std::vector<MapCellSide> sidesToDecide;
	size_t currentOpenings = 0u;
	for (int i = 0; i < 4; i++)
	{
		auto side = static_cast<MapCellSide>(i);
		auto facingAdjacentSide = adjacentCells[i]->GetSide(MapCell::GetOppositeSide(side));
		if (adjacentCells[i]->GetStatus() == MapCellStatus::Placed)
		{
			adding.SetSide(side, facingAdjacentSide);
			if (facingAdjacentSide != MapCellSideStatus::Blocked)
				currentOpenings++;
		}
		else
		{
			sidesToDecide.push_back(side);
		}
	}
	return { adding, sidesToDecide, currentOpenings };
}

void MapGenerator::PlaceStart(MapLayout& layout)		//might add different spawn positions later
{
	layout.PlaceStart(layout.cells.width / 2, layout.cells.height / 5, MapCellSide::Bottom);
}

void MapGenerator::SpawnEnemiesOnInteractables(MapLayout& layout, Level level)
{
	for (const auto& [interactable, position] : layout.interactables)
	{
		layout.SpawnEnemy(position, SpawnRateTranslator::DecideEnemyTypeSpawn(level, interactable));
	}
}
template<typename T>
void MapGenerator::PopulateWithRandomT(MapLayout& layout, Level level)
{
	for (size_t i = 0; i < static_cast<size_t>(layout.cells.width * layout.cells.height); i++)
	{
		auto position = RU.GetUInt(0, layout.cells.width * layout.cells.height - 1);
		if constexpr(std::is_same<T, Enemy>::value)
		{
			layout.SpawnEnemy(position, SpawnRateTranslator::DecideEnemyTypeSpawn(level, Interactable::Empty));
		}
		else if constexpr(std::is_same<T, Trap>::value)
		{
			layout.SpawnTrapRandomDir(position, SpawnRateTranslator::DecideTrapTypeSpawn(level));
		}
		else
		{
			static_assert(false && "unsupported type for PopulateWithRandomT");
		}
	}
}

MapCellPlacementResult MapGenerator::PlaceInteractables(MapLayout& layout, const MapGenerationData& generationData)
{

	if (layout.emptyDeadEnds.size() < GetRequieredInteractables(generationData))
	{
		return MapCellPlacementResult::Fail;					//not enough places to place all interactables
	}
	for (const auto& inter : generationData.interactables)
	{
		for (size_t i = 0; i < inter.second; i++)
		{
			layout.SetCellInteractable(layout.PopRandomEmptyDeadEnd().value(), inter.first);	//not checking optional because of earlier check
		}
	}
	return MapCellPlacementResult::Success;
}

void MapGenerator::ResetGeneration(std::unique_ptr<MapLayout>& layout, size_t& roomsAdded)
{
	layout = std::make_unique<MapLayout>();
	roomsAdded = 0;
	PlaceStart(*layout.get());
}

size_t MapGenerator::GetRequieredInteractables(const MapGenerationData& generationData)
{
	size_t totalInteractables = 0;
	for (const auto& inter : generationData.interactables)
	{
		totalInteractables += inter.second;
	}
	return totalInteractables;
}
