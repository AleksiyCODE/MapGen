#pragma once
#include "MapEnums.h"
#include <vector>

namespace Map
{
	//a cell of map
	class MapCell
	{
	public:
		MapCellSideStatus GetSide(MapCellSide side) const;
		MapCellStatus GetStatus() const;
		Interactable GetInteractable()const;
		Enemy GetEnemy()const;
		size_t GetOpenSideCount() const;
		bool CanSpawnEnemy() const;

		void SetSide(MapCellSide side, MapCellSideStatus sideStatus);
		virtual void SetStatus(MapCellStatus in_status);
		virtual void SetEnemy(Enemy enem);
		virtual void SetTrap(Trap trap, MapCellSide side);
		virtual void SetEnemySpawnAllowed(bool spawnAllowed);
		void SetInteractable(Interactable in_interactable);

		static MapCellSide GetOppositeSide(MapCellSide side);
	protected:
		friend class MapLayout;

		std::vector<MapCellSideStatus> sides{ 4, MapCellSideStatus::Empty };
		std::vector<Trap> trapSides{ 4, Trap::None };
		MapCellStatus status = MapCellStatus::Empty;
		Interactable interactable = Interactable::Empty;
		bool enemySpawnAllowed = false;
		Enemy enemy = Enemy::None;
	};

	static class LockedCell : public MapCell		//this object being const complicates things, so instead it was made
													//impossible to modify by overloading setters 
	{
	public:
		LockedCell();
		virtual void SetEnemySpawnAllowed(bool)override;
		virtual void SetEnemy(Enemy)override;
		virtual void SetStatus(MapCellStatus)override;
	} lockedCell;
}