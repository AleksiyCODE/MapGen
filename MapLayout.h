#pragma once
#include <unordered_map>
#include "MapCell.h"
#include "MapGenerator.h"
#include "MapCellsWrapper.h"

namespace Map
{
	struct StartInformation
	{
		size_t startIndex;
		MapCellSide entrance;
	};

	//level map
	class MapLayout
	{
	public:
		//for debug purposes
		void PrintLayout();

	private:

		void SetCell(size_t index, MapCell cell);
		void SetCell(size_t x, size_t y, MapCell cell);

		std::optional <size_t> PopRandomAvalibleForPlacement();
		std::optional <size_t> PopRandomEmptyDeadEnd();

		void SetCellInteractable(size_t index, Interactable inter);
		void SpawnEnemy(size_t cellIndex, Enemy enemy);
		void SpawnTrapRandomDir(size_t cellIndex, Trap trap);
		void PreventEnemySurrounding(size_t index);
		void DisableEnemySpawnAround(size_t index);
		void FinalizeLayoutCreation();

		MapCellPlacementResult PlaceStart(size_t x, size_t y, MapCellSide entrance);
		bool CheckBounds(size_t x, size_t y) const;
		MapCellPlacementResult PrepareStartPlacing(size_t x, size_t y, MapCellSide entrance);

		bool startPlaced = false;
		StartInformation startInfo{};			
		std::vector<size_t> avalibleForPlacement;
		std::vector<size_t> emptyDeadEnds;
		std::unordered_multimap<Interactable, size_t> interactables;

		MapCellsWrapper cells;

		friend class MapGenerator;
	};
}