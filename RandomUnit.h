#pragma once
#include <random>
#define RU RandomUnit::GetRandomUnit()
class RandomUnit
{
private:
	RandomUnit():rng(rd()){};
public:
	[[nodiscard]] static RandomUnit &GetRandomUnit() { static RandomUnit unit; return unit; }			//thanks Obama										
	[[nodiscard]] size_t GetUInt(size_t low, size_t high);
	[[nodiscard]] float GetFloat(float low, float high);
	[[nodiscard]] float GetFloat0to1();
	[[nodiscard]] bool GetBool();
	std::mt19937& GetMT19937();
private:
	std::random_device rd;
	std::mt19937 rng;
};