#pragma once
#include <vector>
#include <tuple>
#include "SpawnRates.h"

namespace Map
{
	//parameters on how to generate a map
	class MapGenerationData
	{
	public:
		Level level;
		size_t minRoomNumber;
		std::vector<std::pair<Interactable, size_t>> interactables;			//TODO add checks for passing a lot of interactables for a small dungeon
	};
}