#pragma once
#include "SpawnRates.h"

namespace Map
{
	class SpawnRateTranslator
	{		
	public:
		static Enemy DecideEnemyTypeSpawn(Level level, Interactable inter);
		static Trap DecideTrapTypeSpawn(Level level);
	private:
		template <typename CumulativeStorage>
		static decltype(auto) LookupStorage(const CumulativeStorage& cumWeights);
		template <typename CumulativeStorage, typename Storage>
		static void FillStorage(CumulativeStorage& cumWeights, const Storage& weights);
		static std::unordered_map<Level,std::unordered_map<Interactable, std::vector<std::pair<Enemy, float>>>> enemyCumulativeWeightsStorage;
		static std::unordered_map<Level, std::vector<std::pair<Trap, float>>>trapCumulativeWeightsStorage;

		//static void FillTrapStorage(std::vector<std::pair<Trap, float>>& cumWeights, Level level);
	};
}