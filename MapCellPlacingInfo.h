#pragma once
#include "MapCell.h"

namespace Map
{
	//info, needed to properly place a cell on the map 
	class MapCellPlacingInfo
	{
	public:
		MapCellPlacingInfo(MapCell cell, std::vector<MapCellSide> sidesToDecide, size_t currentOpenings)
			:
			cell(cell),
			sidesToDecide(sidesToDecide),
			currentOpenings(currentOpenings)
		{};
		MapCell cell;										//cell that has all predetermened sides placed
		std::vector<MapCellSide> sidesToDecide;				//sides, that should be decided later
		size_t currentOpenings;								//number of corridors, currenly leading to this cell
	};
}